package th.ac.kku.phuangsuwan.pithita;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.tv_hello);
        getScreenOrientation();

    }

    public void getScreenOrientation()
    {
        Display screenOrientation = getWindowManager().getDefaultDisplay();

            if(screenOrientation.getWidth() < screenOrientation.getHeight()){

                //Do something
                tv.setText(getString(R.string.tv_port));

            }else {
                //Do something
                tv.setText(getString(R.string.tv_land));

            }
        }






}
